\documentclass{article}
\usepackage[utf8]{inputenc}

\title{Core Concepts}
\author{Offshore Simulator Centre (OSC) }

\usepackage[square,sort,comma,numbers]{natbib}
\bibliographystyle{plainnat}
\usepackage{graphicx}
\usepackage{svg}
\usepackage{amsmath}
\usepackage{geometry}
\usepackage{pdfpages}
\usepackage{rotating}
\usepackage[hidelinks]{hyperref}
\usepackage{float}
\usepackage{csquotes}
\usepackage{xcolor}   % for \textcolor
\usepackage{listings}
\usepackage[useregional]{datetime2}
\usepackage{rotating}
\usepackage[toc, acronym]{glossaries}
\makenoidxglossaries
%\setacronymstyle{long-short}

\input{../commonAcronyms.txt}


\newcounter{questionCounter}

  \newfloat{question}{tbp}{loa}[questionCounter]
  \setlength{\parskip}{3mm}     % !
  \setlength{\parindent}{3mm}   % !
  \frenchspacing
  \sloppy

\geometry{a4paper, portrait, margin=1in}

\begin{document}

\maketitle
\begin{center}
\DTMnow
\end{center} 

\newpage
\tableofcontents
\newpage

\section{Introduction}
This document serves as documentation of different concepts found in Fathom simulator system.

\section{Core architecture}
This section outlines the core architecture of the OSC simulator system.

\subsection{Schedulers}
The driving engine in the simulator is the scheduler. Everything simulated should be either directly driven (ticked) by the scheduler(s), or synchronized by the scheduler. Currently there are two schedulers: \gls{ps} and \gls{gl}. Actually, \gls{ps} is the only of the two that implement the IScheduler interface. \gls{gl} is in fact an IScheduled, ticked by \gls{ps}.

The scheduler concept is responsible for keeping the time, i.e. making sure that simulation time progresses at a constant rate and that all the scheduled object are ticked at the frequency they have specified. In \gls{gl} the frequency is decided by \gls{gl}'s own "lookahead", the $\Delta t$ that GL provides in its implementation of the IScheduled interface. PS will try to keep this $\Delta t$ between every call to the tick() methods of GL. In fathomConfig.xml we set the GameLoop frequency ($f$) in Hz which is then used to calculate $\Delta t = 1/f$. 

If a component is to run in the simulation as a scheduled object it has to implement the IScheduled interface. By default the scheduled ends up in the PS, but if the class also implements IGameLoopScheduled it will be forwarded by PS to GL. PS ticks GL and GL in turn ticks the IGameLoopScheduled objects.

\begin{figure}[h]
\centering
\includegraphics[width=1.0\columnwidth]{figures/scheduler_agx_sequence.pdf}
\caption{Sequence diagram of the interaction between AGX and the scheduler.}
\label{fig:scheduler_sequence}
\end{figure}

 


\section{AGX Dynamics}
\subsection{Lines}

\subsubsection{Bend Stiffness}
AGX represents bend stiffness as Young's Modulus Bend. In our Aker projects the corresponding value is termed Nominal Bend Stiffness (EI). According to \cite{orcaFlexBend} this means that the value is the product of $E$ and $I$ where $E$ is the Young's modulus and $I$ the second moment of area of the cross section. The value $EI$ represent the bend moment required to bend the line to a curvature of 1 radian per unit length.

To calculate Young's modulus bend, $E_a$ for use in AGX to model a hollow pipe when the $EI$ has been provided:
\begin{align}\label{eq_Ea}
E_a &= \frac{EI}{I_a} \\
&= \frac{64EI}{\pi d^4},
\end{align}
where $I_a$ is the area moment of inertia of a solid, circular tube shape and $d$ is the outer diameter which is equal for the hollow pipe and the simulated pipe in AGX. If you want to know why then read the rest of this section.

We have often been uncertain regarding how the data we receive from the customer is specified, is it really in $EI$, is it $E$ or is it something else? A few rules might help. Young's Modulus $E$ has unit $N/m^2$, while the Nominal Bend Stiffness $EI$ has unit $kN \cdot m^2$. $I$ has unit $m^4$. Often $EI$ is referred to as just "Bending Stiffness" or similar. If the number you have is in $kN \cdot m^2$ it is $EI$ and you can use equation (\ref{eq_Ea}) directly. Make sure to convert from $kN$ to $N$ and use $m$ as the unit for the diameter $d$ before plugging it in.

The expression for $I$ defined by $d$ and not $r$ used in equation (\ref{eq_Ea}) is defined in (\ref{eq:sma_hollow_d}) and the way it relates to $r$ and equation (\ref{eq:sma_hollow_r}) is defined in (\ref{eq:sma_hollow_relation}).

\paragraph{Area Moment of Inertia}
The concept of area moment of inertia, $I$, has many names. From \cite{wiki_sma} we can extract the following quote:
\begin{displayquote}
The 2nd moment of area, also known as moment of inertia of plane area, area moment of inertia, or second area moment, is a geometrical property of an area which reflects how its points are distributed with regard to an arbitrary axis.
\end{displayquote}
Area moment of inertia is related to bend stiffness because Young's modulus, $E$, doesn't account for the shape of the body being bent, just the properties of the material of body the body being bent. In addition to the area of the cross section and the properties of the material, bend stiffness depends on the distribution of the area of the material with respect to the axis around which the bend occurs. An example of this is a horizontal 2" x 4" wooden beam that is to carry a vertical load. We all know that it's load carrying capabilities are different depending on which way you lay it down. This is because $I$ changes with respect to the bend axis. An other example is a pipe versus a solid round stock. If you take the same surface area, i.e. the same amount of material, but distribute it as a thin wall pipe with a large diameter, the result is a significantly stiffer object. This is because even though the $E$ of the material is the same (it is the same material) and the area of the cross section $A$ is the same, $I$ is different and thus the bend stiffness $EI$ is different.

According to wikipedia \cite{wiki_sma_list}, the second moment of area $I$ of a hollow tube, like a hose or a riser, is given by 
\begin{equation}\label{eq:sma_hollow_r}
I_x = I_y = \frac{\pi}{4}(r_2^4 - r_1^4),
\end{equation}
where $r_1$ is the radius of the hollow (air part) and $r_2$ is the radius of the whole tube. See figure \ref{fig:wiki_i_hollow} for an illustration copied from Wikipedia.

If it is easier to use the pipe diameter then, according to \cite{engineeringToolbox_second_moment_of_area}, the equation \ref{eq:sma_hollow_r} becomes
\begin{equation}\label{eq:sma_hollow_d}
I_x = I_y = \frac{\pi}{64}(d_2^4 - d_1^4),
\end{equation}

We get from (\ref{eq:sma_hollow_r}) to (\ref{eq:sma_hollow_d}) like this
\begin{equation}\label{eq:sma_hollow_relation}
\begin{split}
I_x &= I_y = \frac{\pi}{4}(r_2^4 - r_1^4) \\
&=\frac{\pi}{4}((\frac{1}{2}d_2)^4 - (\frac{1}{2}d_1)^4) \\
&=\frac{\pi}{4}(\frac{1}{16}d_2^4 - \frac{1}{16}d_1^4) \\
&=\frac{\pi}{4}\frac{1}{16}(d_2^4 - d_1^4) \\
&=\frac{\pi}{64}(d_2^4 - d_1^4).
\end{split}
\end{equation}

For a real live example we use the riser from the \href{https://offsim.jira.com/browse/OS-4920}{Aker VFN} project. This riser has $d_1 = 152.4 \ mm$ and $d_2 = 235.91 \ mm$. Plugging this into equation \ref{eq:sma_hollow_d} we get
\begin{equation}\label{eq_I_o}
\begin{split}
I &= \frac{\pi}{64}(d_2^4 - d_1^4) \\
&= \frac{\pi}{64}(0.23591^4 - 0.1524^4) \\
&= 1.2556E-4 \ m^4.
\end{split}
\end{equation}

For this specific riser it is also spesified an $EI = 15.059 \ kNm^2$. To get the Young's modulus $E$ we do
\begin{equation}
\begin{split}
E & = \frac{EI}{I} \\
 & =\frac{15.059 kNm^2}{ 1.2556{\rm E-4} \ m^4} \\
 & = 119935.070187339 \ kN/m^2 \\ 
 & = 119935070.187339 \ N/m^2 \\
 & =  1.19935070187339{\rm E}8.
\end{split}
\end{equation}

\paragraph{Accounting for hollow lines in AGX}
Now, since AGX is using solid lines/cylinders when modeling the riser, we need to take this into account when selection the Young's modulus for use in AGX. We define $E_a$ and $I_a$ to be the modulus and area moment of inertia used in AGX. In the same manner we define $E_o$ and $E_o$ to be the corresponding values used in Orca Flex/Aker. Now we want the bend stiffness in AGX, $E_aI_a$ to be equal to $E_oI_o$.
\begin{align}
E_aI_a &=E_oI_o \\
\Rightarrow E_a &= \frac{E_oI_o}{I_a} \\
&= E_o \frac{I_o}{I_a} 
\end{align} 
Note that we have already calculated $E_o$ and $I_o$. Now we need to calculate $I_a$ which is for a solid pipe with the same outer diameter as used when calculating $I_o$ in equation \ref{eq_I_o}. We get the following equation for a solid pipe:
\begin{align}
I_a &= \frac{\pi}{4}r^4 \\
&= \frac{\pi}{4}0.117955^4 \\
&=0.0001520391038544587 \\
&=1.52039E-4
\end{align}
Now we can compute the $E_a$ that we need to plug into AGX:
\begin{align}
E_a &= E_o \frac{I_o}{I_a} \\
&=1.19935070187339{\rm E}8 \frac{1.2556E-4}{1.52039E-4} \\
&=1.19935070187339{\rm E}8 \times 0.825837573998557\\
&=9.904688740085869{\rm E}7
\end{align}


\begin{figure}[h]
\centering
\includegraphics[scale=0.5]{figures/wiki_i_hollow_tube.PNG}
\caption{I for holow tube.}
\label{fig:wiki_i_hollow}
\end{figure}


\subsubsection{A text to Algoryx explaining how we think bend stiffness works}
Often we get the nominal bend stiffness, $EI$, specified for a line to be simulated. As we understand it, this number accounts for the diameter of the line, the materials in it and the distributions of these materials over the cross section of the line.

We want to simulate this line in AGX Dynamics with a bend stiffness as close to the real line as possible. In other words we want the $E_aI_a$ as used by AGX to be equal to the $EI$ given in the project specification. We use the outer diameter of the real line for the AGX wire and calculate $I_a$ based on this. We use an equation for $I$ for a solid cylinder when doing this, as AGX Dynamics simulate wires as solids. We then calculate the Young's Modulus Bend for the AGX wire like this: $E_a = \frac{EI}{I_a}$ to achieve $E_aI_a = EI$. When we then add an AGX wire to the simulation, that has the diameter used when calculating $I_a$ and the calculated $E_a$, we expect it to have bend properties close to the real line.

Moreover, if we have decide on an $EI$ and set up two lines to have the same $EI$, but different diameters and thus also different $E$ and $I$, we expect them to have at least close to the same bend stiffness. Observations we have made suggest that this is not currently the case in AGX Dynamics.


\section{Scene Merge}
A merge tool for git has been developed. It uses scene composer to execute the merge and the following setup allows for plugging the tool into a git environment running in Cygwin.

\subsection{Setup}
We need to register our scene merge tool as a diff tool in git. This involves several steps and will be explained here. First we register the tool in our \textit{.gitconfig} file:
\begin{lstlisting}[frame=single,caption={Set up scene merge in .gitconfig},captionpos=b,label={lst:gitconfig_merge}]
[mergetool "sceneDiff"]
    cmd = sceneDiff "$BASE" "$LOCAL" "$REMOTE" "$MERGED"
    trustExitCode = false
\end{lstlisting}

Then we add a script to be placed in PATH that will start git merge using our merge tool:

\begin{lstlisting}[frame=single,caption={Conveniece script, gsm},captionpos=b,label={lst:gsm},language=bash]
#!/bin/bash
git mergetool --tool=sceneDiff $@
\end{lstlisting}

The \$@ means that any parameters you pass on, line \textit{gsm hei hallo} will pass \textit{hei} and \textit{hallo} on to git merge. In listing \ref{lst:gitconfig_merge} we see the the command that will be executed by git to merge is called \textit{sceneDiff}. We need to add this script to PATH too:

\begin{lstlisting}[frame=single,caption={Merge script, sceneDiff},captionpos=b,label={lst:sceneDiff},language=bash]
#!/bin/bash
pushd ~/gt/w/Ext/poms/scene-diff
./sceneDiff.bat $@
\end{lstlisting}

This goes in to a folder and expects to find the script \textit{sceneDiff.bat} in that folder:
\begin{lstlisting}[frame=single,caption={Maven execution script, sceneDiff.bat},captionpos=b,label={lst:sceneDiff.bat},language=bash]
mvn exec:java -Dexec.args="%*"
\end{lstlisting}
For this to work it needs to find a pom.xml file in the same folder. This file looks like this:
\lstinputlisting[breaklines=true,frame=single,caption={Scene merge pom},captionpos=b,label={lst:merge_pom},language=xml]{sceneMerge/merge_pom.xml}
\textbf{NOTE: }make sure to replace the fathom version with the target version you want the scenes to run in. The scene composer instance that is started up when merging will be in this version.

\subsection{Commands}

\subsubsection{Start commands}
\paragraph{merge start} Starts the merge process. Loads the files provided: BASE, LOCAL and REMOTE. Two diffs will also be built, comparing base to local and remote respectively. A corruption check is run on these files. If it passes you will be told that you can start the merge by running 'merge auto'.

If corruptions are found you can load each of the scenes by running 'merge load base/local/remote', fix any issues and then save the fixed state back to the files using 'merge save current'.
\paragraph{merge start force} As 'merge start', but this one skips the corruption checks.
\paragraph{merge clean} Brings us back to a clean state from which 'merge start' can be run. There is one (big) exception: currently and changes that have been saved back to the files using 'merge save current' will not be reverted.

\subsubsection{Execution commands}
\paragraph{merge auto} The changes that are non conflicting between local and remote will be applied to base.
\paragraph{merge auto added} Applies any "ActorAdded" changes from the two diffs.
\paragraph{merge interactive} Interactively runs through all the conflicts and lets you chose what to do with them.
\paragraph{merge resolve} Writes the current scene to the output file provided by git merge and closes the application. This will cause git to move forward in the merge process. 

\subsubsection{Print commands}
These are commands that will allow you to investigate the state of the merge, remaining, unmerged changes, etc.

\paragraph{merge print categorize} Shows you the type of changes found in local and remote. Like a summary.
\paragraph{merge print remaining [local/remote]} Prints the changes found in either diff that has not yet been merged in.
\paragraph{merge print conflicts} Prints the remaining changes that are in conflict with each other.
\paragraph{merge print invalid} Prints the changes that cannot be applied to the scene in the given state. This can be because the actor a data storage change is for has been removed, etc.

\subsubsection{Apply commands}
Currently there is only one apply command, but it handles a number of different operations.
\paragraph{merge apply changeType [local/remote]} This allow you to apply a change type/category from one of the diffs (local/remote) to the merge result. For example, 'merge apply added local' will take all the actors added in local and apply them to the merge result. The command auto completes/generates suggestions without the changeType pressent, so you can use 'mal' to provide a list of the available operations for the LOCAL diff. 

\printnoidxglossary[type=\acronymtype, title={Abbreviations}, nonumberlist]

\begin{thebibliography}{9}

\bibitem{orcaFlexBend}
  Orca Flex,
  \url{https://www.orcina.com/SoftwareProducts/OrcaFlex/Documentation/Help/Content/html/Linetypes,Structuredata.htm#LineTypeBendStiffness}.

\bibitem{wiki_sma}
  Wikipedia: Second moment of area,
  \url{https://en.wikipedia.org/wiki/Second_moment_of_area}.

\bibitem{wiki_sma_list}
  Wikipedia: List of Second moments of area,
  \url{https://en.wikipedia.org/wiki/List_of_second_moments_of_area}.

\bibitem{engineeringToolbox_second_moment_of_area}
  Enginnering Toolbox: Area Moment of Inertia - Typical Cross Sections I,
  \url{https://www.engineeringtoolbox.com/area-moment-inertia-d_1328.html}.

\bibitem{sjsu}
  Structures and Stiffness,
  \url{https://engineering.sjsu.edu/e10/wp-content/uploads/Structure_Stiffness_S13.pdf}.

\end{thebibliography}


\section{Appendix}

\newpage
\begin{sidewaysfigure}[h]
\centering
\includegraphics[width=\textheight]{figures/scheduler_agx_sequence.pdf}
\caption{Sequence diagram of the interaction between AGX and the scheduler.}
\label{fig:app:scheduler_sequence}
\end{sidewaysfigure}

 

\end{document}
